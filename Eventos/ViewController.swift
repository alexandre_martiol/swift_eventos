//
//  ViewController.swift
//  Eventos
//
//  Created by AlexM on 11/12/14.
//  Copyright (c) 2014 AlexM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var img1: UIImageView!
    var lastScaleFactor:CGFloat = 1
    var netRotation:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Implemented vars of the gestures. They call to the function that will work when the gesture is done
        let pinchGesture:UIPinchGestureRecognizer =
        UIPinchGestureRecognizer(target: self, action: "pinchGesture:")
        img1.addGestureRecognizer(pinchGesture)
        
        var tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
        tapGesture.numberOfTapsRequired = 2;
        img1.addGestureRecognizer(tapGesture)
        
        var rotateGesture = UIRotationGestureRecognizer(target: self, action: "rotateGesture:")
        img1.addGestureRecognizer(rotateGesture)
        
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        img1.addGestureRecognizer(swipeRight)
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        img1.addGestureRecognizer(swipeDown)
        
        var lpgr = UILongPressGestureRecognizer(target: self, action: "respondToLongPress:")
        lpgr.minimumPressDuration = 2.0
        img1.addGestureRecognizer(lpgr)
        
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Implemented methods that work when a gesture is done
    @IBAction func handleTap(sender: UIGestureRecognizer) {
        if (sender.view?.contentMode == UIViewContentMode.ScaleAspectFit){
                sender.view?.contentMode = UIViewContentMode.Center
        }

        else{
            sender.view?.contentMode = UIViewContentMode.ScaleAspectFit
        }
    }
    
    @IBAction func pinchGesture(sender: UIPinchGestureRecognizer) {
        var factor = sender.scale
        
        if (factor > 1) {
            //aumentamos el zoom
            sender.view?.transform = CGAffineTransformMakeScale(
            lastScaleFactor + (factor-1),
            lastScaleFactor + (factor-1));
        } else {
            //reducimos el zoom
            sender.view?.transform = CGAffineTransformMakeScale(
            lastScaleFactor * factor,
            lastScaleFactor * factor);
        }
        
    if (sender.state == UIGestureRecognizerState.Ended){
        if (factor > 1) {
            lastScaleFactor += (factor-1);
        } else {
            lastScaleFactor *= factor; }
        } 
    }
    
    @IBAction func rotateGesture(sender: UIRotationGestureRecognizer) {
        var rotation:CGFloat = sender.rotation
        var transform:CGAffineTransform = CGAffineTransformMakeRotation(rotation + netRotation)
        sender.view?.transform = transform
    
        if (sender.state == UIGestureRecognizerState.Ended){
            netRotation += rotation; 
        } 
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.Right:
                    println("Swiped right")
                case UISwipeGestureRecognizerDirection.Down:
                    println("Swiped down")
                default:
                    break
            }
        }
    }
    
    func respondToLongPress(gesture: UIGestureRecognizer) {
        println("long press")
    }
}

